def eval(formula, truthAssignment)
	formula = formula.split("") if formula.class.name != "Array"
	
	ops = ["!", "&", "|", ">", "-"]
	tA = {
		"I1" => {
					:p1 => false, :p3 => false, :p5 => false,
					:p2 => true, :p4 => true, :p6 => true
				},

		"I2" => {
					:p1 => true, :p3 => true, :p5 => true,
					:p2 => false, :p4 => false, :p6 => false
				},
	}

	if !tA.keys.include?(truthAssignment)
		line = "Invalid truth assignment given: #{truthAssignment}"
		line << "\nValid assignments are one of: #{tA.keys}"
		puts line
		return false
	end

	tA = tA[truthAssignment]

	formulas = {
		"A" => (!tA[:p1] | (tA[:p2] & tA[:p3])) & (tA[:p1] | (tA[:p3] & tA[:p4])),
		"B" => (!tA[:p3] | !tA[:p6]) & (tA[:p3] | (!tA[:p4] | tA[:p1])),
		"C" => (!(tA[:p2] | tA[:p5])) & (!tA[:p2] | tA[:p5]),
		"D" => !(!tA[:p3] | tA[:p6])
	}

	stack = []
	while formula.any?
		sym = formula.pop
		if !formulas.keys.include?(sym) && !ops.include?(sym)
			line = "Unsupported symbol found: #{sym}"
			line << "\nValid symbols are one of: #{ops}."
			line << "\nValid formulas are one of: #{formulas.keys}."
			puts line
			return false
		end

		stack << formulas[sym] if formulas.keys.include?(sym)

		case sym
		when "!"
			arg1 = stack.shift
			stack << !arg1
		when "&"
			arg1 = stack.shift
			arg2 = stack.shift
			stack << (arg1 & arg2)
		when "|"
			arg1 = stack.shift
			arg2 = stack.shift
			stack << (arg1 | arg2)
		when ">"
			arg1 = stack.shift
			arg2 = stack.shift
			stack << (!arg1 | arg2)
		when "-"
			arg1 = stack.shift
			arg2 = stack.shift
			stack << ((!arg1 | arg2) & (!arg2 | arg1))
		end
	end
	return stack.last
end

tests = [
	{ :formula => ">D|A|BC", :assignment => "I1", :result => true },
	{ :formula => ">D|A|BC", :assignment => "I2", :result => true },
	{ :formula => "|AB", :assignment => "I1", :result => false },
	{ :formula => "|AB", :assignment => "I2", :result => true },
	{ :formula => "!|AB", :assignment => "I1", :result => true },
	{ :formula => "!|AB", :assignment => "I2", :result => false },
	{ :formula => "!|C|AB", :assignment => "I1", :result => true },
	{ :formula => "!|C|AB", :assignment => "I2", :result => false },
	{ :formula => ">D>AB", :assignment => "I1", :result => false },
	{ :formula => ">D>AB", :assignment => "I2", :result => true }
]

tests.each { |test|
	line = "Testing: #{test[:formula]}."
	line << " Result: #{eval(test[:formula], test[:assignment])}"
	line << " Expected: #{test[:result]}"
	puts line
}